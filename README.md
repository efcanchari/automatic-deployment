## Automatic deployment

This project contains scripts to deploy AWS infrastructure and Atlassian application by code

1. Infrastructure as code with **Terraform**
2. Provisioning with **Clound-init**
3. Application deployment with **Docker** 

---

### How to use

Before to start you need to have installed in your environment [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) and
 an [AWS account](https://registry.terraform.io/providers/hashicorp/aws/latest/docs) with an enabled user in IAM to execute the script 


1. Go to **terraform** directory
2. Replace content of files: 
    - keys/access_key -> from your AWS account
    - keys/secret_key -> from your AWS account
    - itexp-key -> generated using ssh keygen
    - itexp-key.pub -> generated using ssh keygen
3. Update variables in **main.tf** and **userdata.yaml**
4. Initialice terraform workspace using the comand ```terraform init```
5. Check all changes will be applied next comand ```terraform plan```
6. Execute your deployment with command ```terraform apply```

---

### More details on

[canchari.info](https://canchari.info)
