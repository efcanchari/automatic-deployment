output "instance_ips" {
  value = aws_instance.itexp_server.public_ip
}

output "instance_dns" {
  value = aws_instance.itexp_server.public_dns
}

output "ssh_connection" {
  value = "ssh -i itexp-key ubuntu@${aws_instance.itexp_server.public_dns}"
}

