terraform {
  required_version = ">=0.15.0"
}

provider "aws" {
  region = "eu-west-2"
  allowed_account_ids = ["394286774697"]
  access_key = "${file("keys/access_key")}"
  secret_key = "${file("keys/secret_key")}"
}

resource "aws_key_pair" "itexp_key" {
  key_name = "terraform-key"
  public_key = "${file("itexp-key.pub")}"
}

resource "aws_instance" "itexp_server" {
  ami = "ami-096cb92bb3580c759"
  instance_type = "t2.medium" //"t2.micro"
  associate_public_ip_address = true
  key_name = aws_key_pair.itexp_key.key_name
  user_data = "${file("userdata.yaml")}"
}

resource "aws_security_group" "itexp_sg" {
  name = "webserver_sg"

  ingress {
    description = "Allow all"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    type = "terraform-test-security-group"
  }
}

resource "aws_network_interface_sg_attachment" "itexp_sg_attachment" {
  security_group_id    = aws_security_group.itexp_sg.id
  network_interface_id = aws_instance.itexp_server.primary_network_interface_id
}

resource "aws_route53_record" "jira" {
  zone_id = "<DNS_HOSTED_ZONE_ID>"
  name    = "jira1.<yourdomain.com>."
  type    = "A"
  ttl     = "300"
  records = [aws_instance.itexp_server.public_ip]
}
